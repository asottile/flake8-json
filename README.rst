Flake8-JSON
===========

This is a plugin for Flake8 that will format the output as JSON. By default,
the output is **not** pretty-printed. We would love to add that as a separate
formatter option, though.


Installation
------------

.. code-block:: bash

    pip install flake8-json


Usage
-----

.. code-block:: bash

    flake8 --format=json ...


Competitors
-----------

None that I could find on PyPI


Maintenance Policy
------------------

This project is seeking maintainers. Please open an issue if you're interested
and ensure that you've read the PyCQA's `Code of Conduct`_.


.. _Code of Conduct:
    http://meta.pycqa.org/en/latest/code-of-conduct.html
